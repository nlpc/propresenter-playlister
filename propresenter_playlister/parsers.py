import logging
import re
import zipfile
from functools import wraps
from pathlib import Path
from typing import Any, Callable, NamedTuple

from . import dropbox, esv, gcloud, pco, pro6, rtf
from .cfg import Config, cfg, dict_key
from .fntools import first

VIDEO_ROOT = "/Users/Shared/Renewed%20Vision%20Media/Video"
IMAGE_ROOT = "/Users/Shared/Renewed%20Vision%20Media/Images"


class Assets(NamedTuple):
    files: list[Any]
    cues: list[pro6.etree.Element]


__assets = None


def _assets() -> Assets:
    global __assets
    if __assets is None:
        __assets = Assets([], [])
    return __assets


def _clear_assets() -> None:
    global __assets
    __assets = None


def init(*args: list[Any], **kwargs: dict[str, str]) -> None:
    pass


def finalize(_cfg: Config, outdir: str) -> str:
    cues, slides = _assets().cues, _assets().files
    _clear_assets()
    playlist = pro6.create_playlists(pro6.create_playlist(_cfg.date, *cues))
    txt = pro6.xml_dump(playlist)
    p = f"{outdir}/{_cfg.date}.pro6plx"
    with zipfile.ZipFile(p, mode="w") as archive:
        archive.writestr("data.pro6pl", txt)
        for slide in slides:
            if isinstance(slide, tuple):
                archive.writestr(*slide)
            else:
                title = slide.get("CCLISongTitle").replace(":", "_").replace("'", "")
                archive.writestr(f"{title}.pro6", pro6.xml_dump(slide))
    return p


Slides = list[pro6.etree.Element]
ParserFn = Callable[[dict[str, str], dict[str, Any]], Slides]


def note_assets(parser: ParserFn) -> ParserFn:
    @wraps(parser)
    def wrapper(item: dict[str, str], plan: dict[str, Any]) -> Slides:
        slides = parser(item, plan)
        title, arrangement = dict_key(item), None
        if slides:
            title = slides[0].get("CCLISongTitle")
            e = slides[0].find(".//RVSongArrangement")
            arrangement = None if e is None else e.attrib["uuid"]
        _assets().cues.append(pro6.new_cue(title, arrangement))
        _assets().files.extend(slides)
        return slides

    return wrapper


@note_assets
def parse_library(item: dict[str, str], plan: dict[str, Any]) -> Slides:
    return []


@note_assets
def parse_song(item: dict[str, str], plan: dict[str, Any]) -> Slides:
    title = dict_key(item)
    arrangement = pco.req(f"{plan[title]['links']['self']}/arrangement")
    arrangement = arrangement["data"]["attributes"]
    lyric_parts = arrangement["lyrics"].split("\n\n")
    doc = pro6.new_presentation_doc(title)
    doc.append(pro6.new_timeline())
    doc.append(groups := pro6.new_list("groups"))
    for section, lyrics in zip(lyric_parts[::2], lyric_parts[1::2]):
        rtf_txt = rtf.regular_text(re.sub("\n", rtf.EOL, lyrics))
        rtf_txt = rtf.create_doc(rtf_txt, align=rtf.ALIGN_CENTER)
        groups.append(grouping := pro6.new_slide_grouping(section))
        grouping.append(slides := pro6.new_list("slides"))
        slides.append(pro6.new_slide(rtf_txt))
    doc.append(pro6.new_list("arrangements"))
    return [_cclify_song(doc, plan[title])]


def _cclify_song(
    song: pro6.etree.Element, plan_item: dict[str, Any]
) -> pro6.etree.Element:
    song_id = plan_item["relationships"]["song"]["data"]["id"]
    ccli = pco.req(f"{pco.PCO_API_V2}/songs/{song_id}")["data"]["attributes"]
    song.attrib["CCLIAuthor"] = ccli["author"]
    yr, _, pub = ccli["copyright"].partition(" ")
    song.attrib["CCLICopyrightYear"] = yr
    song.attrib["CCLIPublisher"] = pub
    song.attrib["CCLISongNumber"] = str(ccli["ccli_number"])
    song.attrib["CCLIDisplay"] = "true"
    return song


@note_assets
def parse_dropbox(item: dict[str, str], plan: dict[str, Any]) -> Slides:
    label, normalize = dict_key(item), bool(item.get("normalize", False))
    responders = [
        lambda: _get_media_path(item),
        lambda: _get_description_path(item, plan),
        lambda: dropbox.get_pro6(label, cfg().dropbox_path, normalize),
    ]
    res = first(responders)
    if isinstance(res, list):
        slides = [_pro6ify(path) for path, _ in res]
        files = [pro6.create_presentation(label, slides)] + res
    else:
        files = [res]
    files += _download_media_paths(item.get("download_only"))
    if "background_image" in item:
        _set_background_image(files[0], item["background_image"])
        files += _download_media_paths(item["background_image"])
    return files


def _pro6ify(path: str) -> pro6.etree.Element:
    slide = pro6.new_display_slide(label=Path(path).stem)
    slide.append(pro6.new_list("cues"))
    slide.append(elements := pro6.new_list("displayElements"))
    new_media_element = pro6.new_video_element
    if path.startswith(IMAGE_ROOT):
        new_media_element = pro6.new_image_element
    elements.append(new_media_element(source=f"files://{path}"))
    return slide


def _set_background_image(slide: pro6.etree.Element, image_path: str) -> None:
    title = Path(image_path).stem
    source = str(Path(IMAGE_ROOT) / Path(image_path).name)
    slide.find(".//RVDisplaySlide").append(pro6.new_media_cue(title, source))


def _get_media_path(playlist_item: dict[str, str]) -> list[tuple[str, bytes]]:
    return _download_media_paths(playlist_item["media_path"])


def _download_media_paths(paths: list[str] | str | None) -> list[tuple[str, bytes]]:
    paths = [] if paths is None else paths if isinstance(paths, list) else [paths]
    files = []
    for p in paths:
        meta, res = dropbox.get_file(p)
        root = VIDEO_ROOT if Path(p).suffix in (".mov", ".mp4") else IMAGE_ROOT
        files.append((f"{root}/{Path(p).name}", res.content))
    return files


def _get_description_path(
    playlist_item: dict[str, str], plan: dict[str, Any]
) -> pro6.etree.Element:
    playlist_item_name = dict_key(playlist_item)
    path_name = plan[playlist_item_name]["attributes"]["description"]
    assert path_name is not None  # nosec
    return dropbox.get_pro6(
        path_name, cfg().dropbox_path, bool(playlist_item.get("normalize", False))
    )


@note_assets
def parse_announcements(item: dict[str, str], plan: dict[str, Any]) -> Slides:
    links = gcloud.get_sheet_links(item["gsheets_id"])
    slides: list[pro6.etree.Element] = []
    files: list[tuple[str, bytes]] = []
    for link in [link for link in links if "png" in link]:
        try:
            meta, res = dropbox.get_shared_file(link)
            filename = re.sub(r"\?.*$", "", meta.name)
            label = filename.removesuffix(".png")
            slide = pro6.new_display_slide(label=label)
            slide.append(pro6.new_list("cues"))
            slide.append(elements := pro6.new_list("displayElements"))
            elements.append(
                pro6.new_image_element(source=f"files://{IMAGE_ROOT}/{filename}")
            )
            slides.append(slide)
            files.append((f"{IMAGE_ROOT}/{filename}", res.content))
        except Exception:
            logging.warning(f"failed to load {link}")
            pass
    return [pro6.create_presentation(dict_key(item), slides)] + files


@note_assets
def parse_pc(item: dict[str, str], plan: dict[str, Any]) -> Slides:
    description = plan[dict_key(item)]["attributes"]["description"]
    rtf_txt = rtf.create_doc(rtf.regular_text(description))
    t = f"PC {cfg().date.strftime('%Y%m%d')}"
    return [pro6.create_presentation(t, [pro6.new_slide(rtf_txt)])]


@note_assets
def parse_hc(item: dict[str, str], plan: dict[str, Any]) -> Slides:
    description = plan[dict_key(item)]["attributes"]["description"]
    n, q, a = _parse_qa(description)
    t = f"HC Q{n}"
    qa = [
        rtf.bold_bullet(f"Question {n}"),
        rtf.bullet(q.strip()),
        rtf.regular_bullet(a),
    ]
    rtf_txt = rtf.create_doc(*qa)
    slides = [pro6.new_slide(rtf_txt, is_bullet_reveal=True)]
    return [pro6.create_presentation(t, slides)]


def _parse_qa(text: str) -> tuple[str, str, str]:
    rgx = r"(?:Q\.?\s?(\d+)|(\d+)\.\s*Q)[:.]?\s*(.*?)\n+(?:A[.:]?\s*)?(.*)"
    assert (m := re.match(rgx, text, re.S)) is not None  # nosec
    groups = m.groups()
    return (groups[0] or groups[1], groups[2], groups[3])


@note_assets
def parse_verses(item: dict[str, str], plan: dict[str, Any]) -> Slides:
    description = plan[dict_key(item)]["attributes"]["description"]
    full_ref, slides = _parse_scripture(description)
    return [pro6.create_presentation(full_ref, slides, is_bible_ref=True)]


@note_assets
def parse_notes(item: dict[str, str], plan: dict[str, Any]) -> Slides:
    notes = plan["Notes"]["attributes"]["content"]
    passage, bullets = parse_notes_passage_and_bullets(notes)
    ref = pro6.parse_bible_ref(passage)
    t = f"{passage} Notes"
    return [pro6.create_presentation(t, parse_notes_slides(ref, bullets))]


def parse_notes_passage_and_bullets(notes: str) -> tuple[str, list[str]]:
    lines = [line.strip() for line in notes.split("\n") if line.strip()]
    pfx = "Sermon Passage:"
    assert lines[0].startswith(pfx)  # nosec
    passage = lines[0][len(pfx) :].strip()
    return passage, [line[1:].strip() for line in lines[1:] if line.startswith("-")]


def parse_notes_slides(ref: pro6.BibleRef, bullets: list[str]) -> list[Any]:
    slides = []
    book_ch = f"{ref.book} {ref.chapter}"
    for bullet in bullets:
        bullet = re.sub(r"^[Vv]+\s*", f"{book_ch}:", bullet)
        slides += first(
            [
                lambda: _parse_scripture(bullet)[1],
                lambda: _parse_quote(bullet),
                lambda: _parse_text(bullet),
            ]
        )
    return slides


def _parse_text(s: str) -> list[pro6.etree.Element]:
    return [pro6.new_slide(rtf.create_doc(rtf.regular_text(s) + rtf.EOL))]


def _parse_quote(s: str) -> list[pro6.etree.Element]:
    m = re.match(r"(.*) - (.*)", s)
    if m is None:
        raise AssertionError(f"Invalid scripture ref: {s}")
    quote, cite = (s.strip() for s in m.groups())
    quote = rtf.regular_text(quote or "...") + rtf.EOL
    cite = rtf.ALIGN_RIGHT + rtf.black_text(cite)
    doc = rtf.create_doc(quote, cite)
    return [pro6.new_slide(doc)]


def _parse_scripture(s: str) -> tuple[str, list[Any]]:
    ref = s.replace("–", "-")
    passages = esv.get_passage(ref)
    return f"{ref} (ESV)", [_scripture_to_slide(txt, ref) for txt, ref in passages]


def _scripture_to_rtf(txt: str, ref: str) -> str:
    book_ch, v = ref.split(":")
    book, ch = book_ch.rsplit(maxsplit=1)
    txt = re.sub("\n", rtf.EOL, txt)
    rtf_txt = rtf.superscript(v) + "\n" + rtf.unscript(txt) + rtf.EOL
    rtf_ref = rtf.ALIGN_RIGHT + rtf.black_text(ref)
    return rtf.create_doc(rtf_txt, rtf_ref)


def _scripture_to_slide(txt: str, ref: str) -> pro6.etree.Element:
    rtf_doc = _scripture_to_rtf(txt, ref)
    return pro6.new_slide(rtf_doc, label=f"{ref} (ESV)")
