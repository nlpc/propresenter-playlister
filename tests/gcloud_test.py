import base64
import json
from datetime import datetime, timedelta

import pytest
import responses

from propresenter_playlister import gcloud

FAKE_SHEET_ID = "16eYDBO1QyqB_fpUU0AiEjgxohmtzTjDqZDlglJVECMw"
X_SHARED_LINK = "https://www.dropbox.com/s/{}?dl=0"
X_SHARED_PATHS = ["kvxh5tguc7ug74g/or%20%281200%20%C3%97%20630%20px%29.png"]


@responses.activate
@pytest.mark.usefixtures("fake_env", "fake_gcloud_responses", "mock_httplib2_request")
def test_get_sheet_links():
    links = gcloud.get_sheet_links(FAKE_SHEET_ID)
    assert links == [X_SHARED_LINK.format(p) for p in X_SHARED_PATHS]


@responses.activate
def test_unexpired_creds(fake_env):
    tok_payload = json.loads(base64.b64decode(fake_env["GCLOUD_REFRESH_TOKEN"]))
    tok_payload["expiry"] = datetime.isoformat(datetime.now() + timedelta(days=1))
    fake_env["GCLOUD_REFRESH_TOKEN"] = base64.b64encode(
        json.dumps(tok_payload).encode()
    ).decode()
    creds = gcloud._get_gcreds()
    assert not creds.expired
