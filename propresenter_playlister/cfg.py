from datetime import date
from pathlib import Path
from typing import Any, Callable, NamedTuple

from jinja2 import Environment
from ruamel.yaml import YAML

__cfg = None


class Config(NamedTuple):
    date: date
    service_type: str
    dropbox_path: str
    playlist: list[dict[str, str]] | None = None
    font_color: str = "black"
    parser_module: str | None = None
    variables: dict[str, str] | None = None


def load_config(config_path: str, d: date) -> Config:
    txt = Path(config_path).read_text()
    while True:
        yml = YAML().load(txt)
        env = Environment().from_string(txt)  # nosec
        rendered_txt = env.render(yml)
        if rendered_txt == txt:
            break
        txt = rendered_txt
    return Config(date=d, **yml)


def cfg(config_path: str | None = None, d: date | None = None) -> Config:
    global __cfg
    if config_path is not None:
        d = date.today() if d is None else d
        __cfg = load_config(config_path, d)
    assert __cfg is not None  # nosec
    return __cfg


def dict_item(dct: dict[str, Any], index: int = 0) -> tuple[str, Any]:
    return list(dct.items())[index]


def dict_key(dct: dict[str, Any], index: int = 0) -> Any:
    return dict_item(dct, index)[0]


def dict_val(dct: dict[str, Any], index: int = 0) -> Any:
    return dict_item(dct, index)[1]


def list_to_dict(lst: list[Any], key: Callable[[Any], str]) -> dict[str, Any]:
    return {key(e): e for e in lst}
