from typing import Any, Callable, Iterable


def first(responders: Iterable[Callable[[], Any]]) -> Any:
    for fn in responders:
        try:
            return fn()
        except Exception:  # nosec
            pass
    raise StopIteration()
