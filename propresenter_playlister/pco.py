import json
import os
import urllib.parse
import urllib.request
from datetime import date, datetime, timedelta
from typing import Any, Dict, Generator, List, Tuple

from .cfg import Config, dict_val, list_to_dict

PCO_API_URI = "https://api.planningcenteronline.com"
PCO_API_V2 = f"{PCO_API_URI}/services/v2"
PCO_API_SVC_T = f"{PCO_API_V2}/service_types"
PCO_API_SVC_T_SELECT = f"{PCO_API_SVC_T}?where[name]={{service_type}}"
PCO_API_PLANS = f"{PCO_API_SVC_T}/{{svc_id}}/plans?per_page=2&filter=after&after={{d}}"
PCO_API_PLAN = f"{PCO_API_SVC_T}/{{svc_id}}/plans/{{plan_id}}"
PCO_API_NOTES = f"{PCO_API_PLAN}/notes"
PCO_API_ITEMS = f"{PCO_API_PLAN}/items"
DATE_FMT = "%Y-%m-%dT%H:%M:%SZ"


def get_plan(
    cfg: Config,
) -> Tuple[Generator[Dict[str, str], None, None], Dict[str, Any]]:
    plan_items, plan_notes = get_plan_items(cfg.date, cfg.service_type)
    plan = list_to_dict(plan_items, lambda e: e["attributes"]["title"])
    plan["Notes"] = plan_notes[0]
    return flatten_playlist(plan_items, plan, cfg), plan


def flatten_playlist(
    plan_items: List[Dict[str, Any]], plan: Dict[str, Any], cfg: Config
) -> Generator[Dict[str, str], None, None]:
    assert cfg.playlist  # nosec
    for playlist_item in cfg.playlist:
        if {"start_item", "end_item"}.issubset(playlist_item.keys()):
            s = plan_items.index(plan[playlist_item["start_item"]])
            e = plan_items.index(plan[playlist_item["end_item"]])
            for item in plan_items[:e][s + 1 :]:
                k, v = item["attributes"]["title"], dict_val(playlist_item)
                yield {k: v, **playlist_item}
        else:
            yield playlist_item


def get_plan_items(  # and notes
    d: date, service_type: str
) -> Tuple[List[Dict[str, Any]], List[Dict[str, Any]]]:
    setup_auth()
    service_type = urllib.parse.quote(service_type)
    endpoint = PCO_API_SVC_T_SELECT.format(service_type=service_type)
    svc_id = req(endpoint)["data"][0]["id"]
    endpoint = PCO_API_PLANS.format(svc_id=svc_id, d=d.strftime("%F"))
    plans = [p for p in req(endpoint)["data"] if is_plan_date(p, d)]
    plan_id = plans[0]["id"] if plans else None
    plan_items = req(PCO_API_ITEMS.format(svc_id=svc_id, plan_id=plan_id))
    plan_notes = req(PCO_API_NOTES.format(svc_id=svc_id, plan_id=plan_id))
    return plan_items["data"], plan_notes["data"]


def is_plan_date(plan: Dict[str, Any], d: date) -> bool:
    plan_date = datetime.strptime(plan["attributes"]["last_time_at"], DATE_FMT)
    return plan_date.date() == d


def req(url: str) -> Dict[str, Any]:
    return json.load(urllib.request.urlopen(url))  # nosec


def setup_auth() -> None:
    app_id = os.environ["PCO_APP_ID"]
    secret = os.environ["PCO_SECRET"]
    password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
    password_mgr.add_password(None, PCO_API_URI, app_id, secret)
    auth_handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
    opener = urllib.request.build_opener(auth_handler)
    urllib.request.install_opener(opener)


def upcoming_sunday() -> date:
    d = date.today()
    return d + timedelta(days=6 - d.weekday())
