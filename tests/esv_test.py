from unittest import mock

import pytest

from propresenter_playlister import esv


@pytest.mark.usefixtures("fake_env", "api_responses")
@mock.patch.dict(esv._scripture_cache, {}, clear=True)
def test_get_passage():
    x_cache = {
        "Ephesians 2:4": [
            (
                "But God, being rich in mercy, because of the great love with "
                "which he loved us,",
                "Ephesians 2:4",
            )
        ],
        "Ephesians 2:4-5": [
            (
                "But God, being rich in mercy, because of the great love with "
                "which he loved us,",
                "Ephesians 2:4",
            ),
            (
                "even when we were dead in our trespasses, made us alive "
                "together with Christ—by grace you have been saved— (ESV)",
                "Ephesians 2:5",
            ),
        ],
        "Ephesians 2:5": [
            (
                "even when we were dead in our trespasses, made us alive "
                "together with Christ—by grace you have been saved— (ESV)",
                "Ephesians 2:5",
            )
        ],
    }
    passages = esv.get_passage("Ephesians 2:4-5")
    assert passages == x_cache["Ephesians 2:4-5"]
    assert esv._scripture_cache == x_cache


@pytest.mark.usefixtures("fake_env", "api_responses")
@mock.patch.dict(esv._scripture_cache, {}, clear=True)
def test_get_passage_preserves_newlines():
    passages = esv.get_passage("Hebrews 2:5-3:1")
    assert "\n" in passages[1][0]
    assert passages[-1][1] == "Hebrews 3:1"


@pytest.mark.usefixtures("fake_env", "api_responses")
@mock.patch.dict(esv._scripture_cache, {}, clear=True)
def test_get_passage_registers_whole_chapter_verses():
    passages = esv.get_passage("Psalm 84:1-12")
    assert len(passages) == 12
    assert passages[0][1] == "Psalm 84:1"


@pytest.mark.usefixtures("fake_env", "api_responses")
@mock.patch.dict(esv._scripture_cache, {}, clear=True)
def test_get_passage_non_contiguous_verses():
    passages = esv.get_passage("Psalm 90:14,17")
    assert len(passages) == 2
