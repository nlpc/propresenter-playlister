import base64
import os
import re
from typing import Any

import dropbox
import lxml.etree  # nosec
import requests

from .cfg import cfg

DOC_PATH = "__Documents/Default"
RTF_WHITE = r"\\red255\\green255\\blue255"
RTF_BLACK = r"\\red0\\green0\\blue0"
RTF_CSGRAY = r"\\csgray\\c100000"
RTF_CSSRGB = r"\\cssrgb\\c0\\c0\\c0"

_dbx = None


def get_pro6(title: str, dropbox_path: str, normalize: bool = False) -> lxml.etree:
    p = f"{dropbox_path}/{DOC_PATH}/{title}.pro6"
    resp = get_file(p)[1]
    doc = lxml.etree.fromstring(resp.text)  # nosec
    if normalize:
        doc = update_background_color(update_title(update_font(doc), title))
    return doc


def get_file(dropbox_path: str) -> tuple[Any, requests.models.Response]:
    return _get_dbx().files_download(dropbox_path)


def get_shared_file(
    dropbox_path: str,
) -> tuple[dropbox.sharing.SharedLinkMetadata, requests.models.Response]:
    return _get_dbx().sharing_get_shared_link_file(dropbox_path)


def _get_dbx() -> Any:
    global _dbx
    if not _dbx:
        _dbx = dropbox.Dropbox(
            oauth2_refresh_token=os.environ["DROPBOX_REFRESH_TOKEN"],
            app_key=os.environ["DROPBOX_KEY"],
            app_secret=os.environ["DROPBOX_SECRET"],
        )
    return _dbx


def update_background_color(xml: lxml.etree) -> lxml.etree:
    xml.attrib["drawingBackgroundColor"] = "true"
    xml.attrib["backgroundColor"] = "0 0 0 0"
    for e in xml.findall(".//RVDisplaySlide"):
        e.attrib["drawingBackgroundColor"] = "true"
        e.attrib["backgroundColor"] = "0 0 0 0"
    return xml


def update_title(xml: lxml.etree, title: str) -> lxml.etree:
    if not xml.get("CCLISongTitle"):
        xml.set("CCLISongTitle", title)
    return xml


def update_font(xml: lxml.etree) -> lxml.etree:
    for e in xml.findall(".//RVTextElement"):
        e.attrib["fromTemplate"] = "true"
        e.attrib["drawingShadow"] = "false"
    is_wb = cfg().font_color == "white"
    for e in [s for s in xml.findall(".//NSString") if is_rtf_string(s)]:
        rtf = base64.b64decode(e.text).decode()
        color, xcolor = (RTF_WHITE, RTF_CSGRAY) if is_wb else (RTF_BLACK, RTF_CSSRGB)
        rtf = re.sub(r"(\\colortbl;[^;]*;)([^;]*)", r"\1" + color, rtf)
        rtf = re.sub(r"(\\expandedcolortbl;;)([^;]*)", r"\1" + xcolor, rtf)
        e.text = base64.b64encode(rtf.encode()).decode()
    return xml


def is_rtf_string(element: lxml.etree) -> bool:
    return element.attrib.get("rvXMLIvarName", "") == "RTFData"
