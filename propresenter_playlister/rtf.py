from typing import Optional

from .cfg import cfg

EOL = "\\\n"
RTF_FLAVOR = r"\rtf1\ansi\ansicpg1252\cocoartf2636"
RTF_ENCODING = r"\cocoatextscaling0\cocoaplatform0"
FONT_TABLE = r"{\fonttbl\f0\fnil\fcharset0 Lato-Regular;\f1\fnil\fcharset0 Lato-Light;}"
COLOR_TABLE = r"{\colortbl;\red255\green255\blue255;\red0\green0\blue0;}"
COLOR_TABLE_WHITE = r"{\colortbl;\red255\green255\blue255;\red255\green255\blue255;}"
EXPANDED_COLOR_TABLE = r"{\*\expandedcolortbl;;\cssrgb\c0\c0\c0;}"
EXPANDED_COLOR_TABLE_WHITE = r"{\*\expandedcolortbl;;\csgray\c100000;}"
ALIGN_JUSTIFY = "\\pard\\pardirnatural\\qj\\partightenfactor0\n"
ALIGN_RIGHT = "\\pard\\pardirnatural\\qr\\partightenfactor0\n"
ALIGN_CENTER = "\\pard\\pardirnatural\\qc\\partightenfactor0\n"
HEADER_BW = (RTF_FLAVOR + "\n" + RTF_ENCODING + FONT_TABLE + "\n") + (
    COLOR_TABLE + "\n" + EXPANDED_COLOR_TABLE + "\n"
)
HEADER_WB = (RTF_FLAVOR + "\n" + RTF_ENCODING + FONT_TABLE + "\n") + (
    COLOR_TABLE_WHITE + "\n" + (EXPANDED_COLOR_TABLE_WHITE + "\n")
)


def create_doc(*rtfs: str, align: Optional[str] = None) -> str:
    is_wb = cfg().font_color == "white"
    hdr = HEADER_WB if is_wb else HEADER_BW
    hdr += align or ALIGN_JUSTIFY
    return "{" + hdr + "".join(rtfs) + "}"


def bold_bullet(s: str) -> str:
    return r"\f0\b\fs116 \cf2 " + escape(s)


def regular_bullet(s: str) -> str:
    return f"\\f1\\b0 \\\n\\\n{escape(s)}"


def superscript(s: str) -> str:
    return f"\\f1\\fs56 \\cf2 \\super2 {s}"


def unscript(s: str) -> str:
    return f"\\fs112 \\nosupersub {escape(s)}"


def bullet(s: str) -> str:
    return f"\\\n{escape(s)}"


def regular_text(s: str) -> str:
    return f"\\f1\\fs112 \\cf2 {escape(s)}"


def black_text(s: str) -> str:
    return f"\\cf2 {escape(s)}"


def escape(s: str) -> str:
    return (
        s.replace("'", r"\'27")
        .replace('"', r"\'22")
        .replace("‘", r"\'91")
        .replace("’", r"\'92")
        .replace("“", r"\'93")
        .replace("”", r"\'94")
        .replace("…", r"\'85")
        .replace("–", r"\'96")
        .replace("—", r"\'97")
    )
