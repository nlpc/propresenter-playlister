import base64
import json
import os
from typing import List

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build


def _get_gcreds() -> Credentials:
    creds = json.loads(base64.b64decode(os.environ["GCLOUD_REFRESH_TOKEN"]))
    gcreds = Credentials.from_authorized_user_info(creds)
    if gcreds.expired:
        gcreds.refresh(Request())
    return gcreds


def get_sheet_links(gsheets_id: str) -> List[str]:
    sheet = build("sheets", "v4", credentials=_get_gcreds()).spreadsheets()
    fields = "sheets/data/rowData/values/hyperlink"
    d = sheet.get(spreadsheetId=gsheets_id, fields=fields).execute()
    rows = [r["values"] for r in d["sheets"][0]["data"][0]["rowData"] if r]
    return [c["hyperlink"] for r in rows for c in r if c]
