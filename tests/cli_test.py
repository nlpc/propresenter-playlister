from datetime import date
from unittest import mock

import pytest
import responses

from propresenter_playlister import cli


@pytest.mark.usefixtures(
    "api_responses",
    "fake_env",
    "mock_urlopen",
    "fake_dropbox_responses",
    "fake_gcloud_responses",
    "mock_httplib2_request",
)
@mock.patch("propresenter_playlister.pco.date")
@responses.activate
def test_pro6_playlister_main(mock_date, tmpdir):
    mock_date.today.return_value = date(2022, 1, 22)
    assert cli.main([f"-d{tmpdir}", "--config=service-1.yml"]) == 0
    assert (tmpdir / "2022-01-23.pro6plx").exists()


@pytest.mark.usefixtures(
    "api_responses", "fake_env", "mock_urlopen", "fake_dropbox_responses"
)
@mock.patch("propresenter_playlister.pco.date")
@responses.activate
def test_pro6_playlister_main_alt_config(mock_date, tmpdir):
    mock_date.today.return_value = date(2023, 1, 29)
    assert cli.main([f"-d{tmpdir}", "-cservice-2.yml"]) == 0
    assert (tmpdir / "2023-01-29.pro6plx").exists()
