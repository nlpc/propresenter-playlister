# Getting started

## One-time setup

1. clone this repo
   ```
   git clone https://gitlab.com/nlpc/propresenter-playlister.git
   cd propresenter-playlister
   ```
2. install tox and pre-commit in a virtual environment
   ```
   python -m venv venv
   source venv/bin/activate
   pip install tox pre-commit
   ```
3. install the pre-commit hook to enforce conventional commits
   ```
   pre-commit install --hook-type commit-msg
   ```

## Development

Don't forget to activate your virtual environment if you haven't already
```
source venv/bin/activate
```
and run
```
tox -py,pre-commit
```
This will run the test suite as well as static analysis tools to enforce
code quality.
