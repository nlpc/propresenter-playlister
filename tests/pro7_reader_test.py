import pytest

from propresenter_playlister import __version__, pro7_reader


def test_version(capsys):
    with pytest.raises(SystemExit):
        pro7_reader.main(["-V"])
    assert capsys.readouterr().out.strip() == __version__


def test_filename_as_cmdline_arg():
    args = pro7_reader.parse_args(["test_file.pro"])
    assert args.pro_file == "test_file.pro"


# read pro file to get file structure and default values
# write pro file with a given quote
