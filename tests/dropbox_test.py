import base64
from datetime import date

import pytest
import responses

from propresenter_playlister import cfg, dropbox

TEST_CFG = """\
service_type: Sunday First Service
dropbox_path: /NLPC ProPresenter 2018/Sync 2018 - 1st Service
font_color: {font_color}
playlist:
  - Call to Worship: verses\
"""


@responses.activate
@pytest.mark.usefixtures("fake_env")
@pytest.mark.parametrize(
    "font_color,font_proof",
    [("white", r"red255")],
)
def test_get_pro6_from_title_and_set_font(
    font_color, font_proof, fake_dropbox_responses, tmp_path
):
    dbx_urls = fake_dropbox_responses
    cfg_path = tmp_path / "test_cfg.yml"
    cfg_path.write_text(TEST_CFG.format(**locals()))
    cfg.cfg(cfg_path, date(2022, 1, 23))
    xml = dropbox.get_pro6("dummy", cfg.cfg().dropbox_path)
    responses.assert_call_count(dbx_urls.download, 1)
    txt = base64.b64decode(
        xml.find(".//RVTextElement").find(".//NSString").text
    ).decode()
    assert font_proof in txt
