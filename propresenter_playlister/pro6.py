import re
from base64 import b64encode
from datetime import date, datetime
from typing import List, NamedTuple, Optional
from uuid import uuid1

from lxml import etree  # nosec

# Playlist helpers


def create_playlists(*playlists: etree.Element) -> etree.Element:
    doc = new_playlist_doc()
    doc.append(root := new_playlist_node())
    doc.append(new_list("deletions"))
    doc.append(new_list("tags"))
    root.append(array := new_list())
    for playlist in playlists:
        array.append(playlist)
    return doc


def create_playlist(d: date, *cues: etree.Element) -> etree.Element:
    node = new_playlist_node(f"{d.month:02}_{d.day:02}_{d.year}")
    node.append(cue_list := new_list())
    for cue in cues:
        cue_list.append(cue)
    node.append(new_list("events"))
    return node


def new_playlist_doc() -> etree.Element:
    kwargs = dict(versionNumber="600", os="2", buildNumber="100991749")
    return etree.Element("RVPlaylistDocument", **kwargs)


def new_playlist_node(displayName: Optional[str] = None) -> etree.Element:
    return etree.Element(
        "RVPlaylistNode",
        displayName=displayName or "root",
        UUID=str(uuid1()).upper(),
        smartDirectoryURL="",
        modifiedDate=timestamp(),
        type="0" if displayName is None else "3",
        isExpanded="false",
        hotFolderType="2",
        **({"rvXMLIvarName": "rootNode"} if displayName is None else {}),
    )


# presentation helpers


def create_presentation(
    title: str, slides: List[etree.Element], is_bible_ref: bool = False
) -> etree.Element:
    presentation = new_presentation_doc(title)
    presentation.append(new_timeline())
    sfx = " (ESV)"
    bible_ref = None
    if is_bible_ref and title.endswith(sfx):
        bible_ref = title[: -len(sfx)]
    if bible_ref:
        presentation.append(new_bible_ref(bible_ref))
    presentation.append(groups := new_list("groups"))
    groups.append(grouping := new_slide_grouping(bible_ref))
    grouping.append(_slides := new_list("slides"))
    _slides.extend(slides)
    grouping.append(new_list("arrangements"))
    return presentation


def new_ivar(cls: str, name: str, text: Optional[str] = None) -> etree.Element:
    node = etree.Element(cls, rvXMLIvarName=name)
    if text is not None:
        node.text = text
    return node


def new_presentation_doc(title: str = "", category: str = "Song") -> etree.Element:
    return etree.Element(
        "RVPresentationDocument",
        CCLIArtistCredits="",
        CCLIAuthor="",
        CCLICopyrightYear="",
        CCLIDisplay="false",
        CCLIPublisher="",
        CCLISongNumber="",
        CCLISongTitle=title,
        backgroundColor="0 0 0 0",
        buildNumber="100991749",
        category=category,
        chordChartPath="",
        docType="0",
        drawingBackgroundColor="true",
        height="720",
        lastDateUsed=timestamp(),
        notes="",
        os="2",
        resourcesDirectory="",
        selectedArrangementID="",
        usedCount="0",
        uuid=str(uuid1()).upper(),
        versionNumber="600",
        width="1280",
    )


def new_slide(
    rtf_text: str, is_bullet_reveal: bool = False, label: str = ""
) -> etree.Element:
    encoded_text = b64encode(rtf_text.encode()).decode()
    slide = new_display_slide(label)
    slide.append(new_list("cues"))
    slide.append(elements := new_list("displayElements"))
    elements.append(e := new_text_element(is_bullet_reveal))
    s = new_ivar("NSString", "RTFData")
    e.append(s)
    s.text = encoded_text
    return slide


def new_display_slide(label: str = "") -> etree.Element:
    return etree.Element(
        "RVDisplaySlide",
        UUID=str(uuid1()).upper(),
        backgroundColor="0 0 0 0",
        chordChartPath="",
        drawingBackgroundColor="true",
        enabled="true",
        highlightColor="0 0 0 0",
        hotKey="",
        label=label,
        notes="",
        socialItemCount="1",
    )


def new_image_element(
    source: str,
    fill_color: Optional[str] = None,
    scale_behavior: Optional[str] = None,
    rect_position: Optional[str] = None,
) -> etree.Element:
    e = etree.Element(
        "RVImageElement",
        UUID=str(uuid1()).upper(),
        bezelRadius="0.000000",
        displayDelay="0.000000",
        displayName="ImageElement",
        drawingFill="false",
        drawingShadow="false",
        drawingStroke="false",
        fillColor="" if fill_color is None else fill_color,
        flippedHorizontally="false",
        flippedVertically="false",
        format="PNG image",
        fromTemplate="false",
        imageOffset="{0, 0}",
        locked="false",
        manufactureName="",
        manufactureURL="",
        opacity="1.000000",
        persistent="false",
        rotation="0.000000",
        rvXMLIvarName="element",
        scaleBehavior="0" if scale_behavior is None else scale_behavior,
        scaleSize="{1, 1}",
        source=source,
        typeID="0",
    )
    e.append(
        new_ivar(
            "RVRect3D",
            "position",
            "{0 0 0 1280 720}" if rect_position is None else rect_position,
        )
    )
    return e


def new_video_element(source: str) -> etree.Element:
    e = etree.Element(
        "RVVideoElement",
        UUID=str(uuid1()).upper(),
        audioVolume="1.000000",
        bezelRadius="0.000000",
        displayDelay="0.000000",
        displayName="VideoElement",
        drawingFill="false",
        drawingShadow="false",
        drawingStroke="false",
        endPoint="6040",
        fieldType="0",
        fillColor="0 0 0 0",
        flippedHorizontally="false",
        flippedVertically="false",
        format="'avc1'",
        frameRate="25.000000",
        fromTemplate="false",
        imageOffset="{0, 0}",
        inPoint="0",
        locked="false",
        manufactureName="",
        manufactureURL="",
        naturalSize="{1920, 1080}",
        opacity="1.000000",
        outPoint="6040",
        persistent="false",
        playRate="1.000000",
        playbackBehavior="0",
        rotation="0.000000",
        scaleBehavior="0",
        scaleSize="{1, 1}",
        source=source,
        timeScale="1000",
        typeID="0",
    )
    e.append(new_ivar("RVRect3D", "position", "{0 0 0 1280 720}"))
    return e


def new_text_element(is_bullet_reveal: bool = False) -> etree.Element:
    e = etree.Element(
        "RVTextElement",
        UUID=str(uuid1()).upper(),
        additionalLineFillHeight="0.000000",
        adjustsHeightToFit="false",
        bezelRadius="0.000000",
        displayDelay="0.000000",
        displayName="TextElement",
        drawLineBackground="false",
        drawingFill="false",
        drawingShadow="false",
        drawingStroke="false",
        fillColor="0 0 0 0",
        fromTemplate="true",
        lineBackgroundType="0",
        lineFillVerticalOffset="0.000000",
        locked="false",
        opacity="1.000000",
        persistent="false",
        revealType="1" if is_bullet_reveal else "0",
        rotation="0.000000",
        source="",
        textSourceRemovalLineReturnsOption="false",
        typeID="0",
        useAllCaps="false",
        verticalAlignment="1",
    )
    e.append(new_ivar("RVRect3D", "position", "{40 38 0 1200 594}"))
    e.append(new_shadow("5.000000|0 0 0 0.5|{3.5355338454246521, -3.5355338454246521}"))
    e.append(new_stroke())
    return e


def new_slide_grouping(name: Optional[str] = None) -> etree.Element:
    return etree.Element(
        "RVSlideGrouping",
        color="0.2637968361377716 0.2637968361377716 0.2637968361377716 1",
        name=name if name else "Group",
        uuid=str(uuid1()).upper(),
    )


def new_timeline() -> etree.Element:
    timeline = etree.Element(
        "RVTimeline",
        duration="0.000000",
        loop="false",
        playBackRate="1.000000",
        rvXMLIvarName="timeline",
        selectedMediaTrackIndex="0",
        timeOffset="0.000000",
    )
    timeline.append(new_list("timeCues"))
    timeline.append(new_list("mediaTracks"))
    return timeline


def new_bible_ref(ref: str) -> etree.Element:
    r = parse_bible_ref(ref)
    return _new_bible_ref(
        book_index=str(BOOKS.index(r.book)),
        book_name=r.book,
        chapter_end=str(r.chapter_end),
        chapter_start=str(r.chapter),
        translation_abbreviation="ESV",
        translation_name="English Standard Version",
        verse_end=str(r.verse_end),
        verse_start=str(r.verse),
    )


def _new_bible_ref(
    book_index: str = "0",
    book_name: str = "",
    chapter_end: str = "0",
    chapter_start: str = "0",
    translation_abbreviation: str = "",
    translation_name: str = "",
    verse_end: str = "0",
    verse_start: str = "0",
) -> etree.Element:
    return etree.Element(
        "RVBibleReference",
        bookIndex=book_index,
        bookName=book_name,
        chapterEnd=chapter_end,
        chapterStart=chapter_start,
        rvXMLIvarName="bibleReference",
        translationAbbreviation=translation_abbreviation,
        translationName=translation_name,
        verseEnd=verse_end,
        verseStart=verse_start,
    )


# rv helpers


def new_cue(title: str, arrangement: Optional[str] = None) -> etree.Element:
    filename = title.replace(":", "_").replace("'", "")
    return etree.Element(
        "RVDocumentCue",
        UUID=str(uuid1()).upper(),
        displayName=title,
        actionType="0",
        enabled="false",
        timeStamp="0.000000",
        delayTime="0.000000",
        filePath=f"~/Documents/ProPresenter6/{filename}.pro6",
        selectedArrangementID=arrangement or "",
    )


def new_media_cue(title: str, source: str) -> etree.Element:
    e = etree.Element(
        "RVMediaCue",
        UUID=str(uuid1()).upper(),
        actionType="0",
        alignment="4",
        behavior="1",
        dateAdded="",
        delayTime="0.000000",
        displayName=title,
        enabled="false",
        nextCueUUID="",
        rvXMLIvarName="backgroundMediaCue",
        tags="",
        timeStamp="0.000000",
    )
    e.append(
        new_image_element(
            source,
            fill_color="0 0 0 0",
            scale_behavior="3",
            rect_position="{0 0 0 0 0}",
        )
    )
    e.append(new_shadow("0.000000|0 0 0 0.3333333432674408|{4, -4}"))
    e.append(new_stroke())
    return e


def new_shadow(shadow: str) -> etree.Element:
    e = new_ivar("shadow", "shadow")
    e.text = shadow
    return e


def new_stroke() -> etree.Element:
    e = new_ivar("dictionary", "stroke")
    color = etree.Element("NSColor", rvXMLDictionaryKey="RVShapeElementStrokeColorKey")
    color.text = "0 0 0 1"
    n = etree.Element(
        "NSNumber", hint="float", rvXMLDictionaryKey="RVShapeElementStrokeWidthKey"
    )
    n.text = "1.000000"
    e.append(color)
    e.append(n)
    return e


def new_list(name: Optional[str] = None) -> etree.Element:
    return etree.Element("array", rvXMLIvarName=name or "children")


# util


def timestamp() -> str:
    dt = datetime.now().astimezone().strftime("%FT%T%z")
    return ":".join((dt[:-2], dt[-2:]))


def xml_dump(xml: etree.Element) -> str:
    kwargs = dict(xml_declaration=True, encoding="utf-8", standalone=True)
    kwargs.update(pretty_print=True)
    return etree.tostring(xml, **kwargs).decode()


class BibleRef(NamedTuple):
    book: str
    chapter: int
    verse: Optional[int] = None
    verse_end: Optional[int] = None
    chapter_end: Optional[int] = None


def parse_bible_ref(ref: str) -> BibleRef:
    m = re.match(r"((?:\d+ )?\w+) (\d+)(?::(.*))?", ref)
    assert m is not None  # nosec
    book, chapter, verses = m.groups()
    args = []
    verse = verse_end = None
    chapter_end = chapter
    if verses is not None:
        ranges = verses.split(",") if "," in verses else verses.split(";")
        verse = ranges[0].split("-")[0]
        verse_end = ranges[-1].split("-")[-1]
        chapter_end = chapter
        if ":" in verse_end:
            chapter_end, verse_end = verse_end.split(":")
    try:
        args = [int(a) if a else None for a in [verse, verse_end, chapter_end]]
    except ValueError:
        args = [int(a) if a else None for a in [verse, verse, chapter]]
    return BibleRef(book, int(chapter), *args)


BOOKS = [
    *["Genesis", "Exodus", "Leviticus", "Numbers", "Deuteronomy", "Joshua"],
    *["Judges", "Ruth", "1 Samuel", "2 Samuel", "1 Kings", "2 Kings"],
    *["1 Chronicles", "2 Chronicles", "Ezra", "Nehemiah", "Esther", "Job"],
    *["Psalm", "Proverbs", "Ecclesiastes", "Song of Solomon", "Isaiah"],
    *["Jeremiah", "Lamentations", "Ezekiel", "Daniel", "Hosea", "Joel", "Amos"],
    *["Obadiah", "Jonah", "Micah", "Nahum", "Habakkuk", "Zephaniah", "Haggai"],
    *["Zechariah", "Malachi", "Matthew", "Mark", "Luke", "John", "Acts"],
    *["Romans", "1 Corinthians", "2 Corinthians", "Galatians", "Ephesians"],
    *["Philippians", "Colossians", "1 Thessalonians", "2 Thessalonians"],
    *["1 Timothy", "2 Timothy", "Titus", "Philemon", "Hebrews", "James"],
    *["1 Peter", "2 Peter", "1 John", "2 John", "3 John", "Jude", "Revelation"],
]
