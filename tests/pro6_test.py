import re
from datetime import date

import pytest

from propresenter_playlister import pro6

EMPTY_PLAYLIST = (
    r"<\?xml version='1.0' encoding='utf-8' standalone='yes'\?>\s*"
    r'<RVPlaylistDocument versionNumber="600" os="2" buildNumber="100991749">\s*'
    r'<RVPlaylistNode displayName="root" '
    r'UUID="[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}" '
    r'smartDirectoryURL="" modifiedDate="\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d[-+]\d\d:\d\d" '
    r'type="0" isExpanded="false" hotFolderType="2" rvXMLIvarName="rootNode">\s*'
    r'<array rvXMLIvarName="children">\s*'
    r'<RVPlaylistNode displayName="\d\d_\d\d_\d\d\d\d" '
    r'UUID="[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}" '
    r'smartDirectoryURL="" modifiedDate="\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d[-+]\d\d:\d\d" type="3" '
    r'isExpanded="false" hotFolderType="2">\s*'
    r'<array rvXMLIvarName="children"/>\s*'
    r'<array rvXMLIvarName="events"/>\s*'
    r"</RVPlaylistNode>\s*"
    r"</array>\s*"
    r"</RVPlaylistNode>\s*"
    r'<array rvXMLIvarName="deletions"/>\s*'
    r'<array rvXMLIvarName="tags"/>\s*'
    r"</RVPlaylistDocument>"
)


def test_create_playlist_for_given_day():
    xml = pro6.create_playlists(pro6.create_playlist(date(2022, 1, 23)))
    assert re.match(EMPTY_PLAYLIST, pro6.xml_dump(xml)), f"got {xml}"


@pytest.mark.parametrize(
    "ref,x",
    [
        ("John 3:16", pro6.BibleRef("John", 3, 16, 16, 3)),
        ("James 1:2-4", pro6.BibleRef("James", 1, 2, 4, 1)),
        ("James 1:2-4", pro6.BibleRef("James", 1, 2, 4, 1)),
        ("1 John 1:10-2:1", pro6.BibleRef("1 John", 1, 10, 1, 2)),
        ("Psalm 81", pro6.BibleRef("Psalm", 81, None, None, 81)),
        ("Psalm 103:1-5,20-22", pro6.BibleRef("Psalm", 103, 1, 22, 103)),
        ("Psalm 118:24,28-29", pro6.BibleRef("Psalm", 118, 24, 29, 118)),
        ("Psalm 90:14,17", pro6.BibleRef("Psalm", 90, 14, 17, 90)),
    ],
)
def test_parse_bible_ref(ref, x):
    assert pro6.parse_bible_ref(ref) == x
