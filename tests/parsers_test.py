import base64
import datetime
import textwrap

import pytest
import responses

from propresenter_playlister import cfg, parsers


@pytest.fixture(autouse=True)
def clear_assets():
    parsers._clear_assets()


@responses.activate
@pytest.mark.usefixtures(
    "fake_env",
    "fake_dropbox_responses",
    "fake_gcloud_responses",
    "mock_httplib2_request",
)
def test_parse_announcements(tmp_path):
    fake_cfg = """\
      service_type: Sunday First Service
      dropbox_path: NLPC ProPresenter 2018/Sync 2018 - 1st Service
      playlist:
        - Announcements - TEST: announcements
          gsheets_id: 16eYDBO1QyqB_fpUU0AiEjgxohmtzTjDqZDlglJVECMw\
    """
    cfg_path = tmp_path / "fake_cfg.yml"
    cfg_path.write_text(textwrap.dedent(fake_cfg))
    cfg.cfg(cfg_path, datetime.date(2022, 1, 23))
    parsers.parse_announcements(cfg.cfg().playlist[0], {})
    [doc, file] = parsers._assets().files
    e = doc.find(".//RVImageElement")
    assert "Users/Shared/Renewed%20Vision%20Media" in e.attrib["source"]
    assert "png" in file[0]


@responses.activate
@pytest.mark.usefixtures("fake_env")
def test_parse_song(tmp_path, api_responses):
    cfg.cfg("service-1.yml", datetime.date(2022, 1, 23))
    fake_plan = {
        "Rejoice": {
            "relationships": {"song": {"data": {"id": 16088283}}},
            "links": {
                "self": (
                    "https://api.planningcenteronline.com/services/v2/"
                    "service_types/764160/plans/66115198/items/891937097"
                )
            },
        }
    }
    parsers.parse_song({"Rejoice": "song"}, fake_plan)
    e = parsers._assets().files
    assert e[0].attrib["CCLISongTitle"] == "Rejoice"


def test_parse_text(tmp_path):
    cfg.cfg("service-1.yml")
    slides = parsers._parse_text("Hi")
    rtf_data = slides[0].getchildren()[1].getchildren()[0].getchildren()[-1]
    txt = base64.b64decode(rtf_data.text).decode()
    assert "Hi" in txt


def test_first_exhaust_responders():
    def crash():
        raise AssertionError()

    with pytest.raises(StopIteration):
        parsers.first([crash])


@responses.activate
@pytest.mark.usefixtures("fake_env", "api_responses")
def test_parse_scriptures():
    cfg.cfg("service-1.yml", datetime.date(2022, 1, 23))
    res = parsers._parse_scripture("Psalm 34:18;1 Peter 2:24")
    assert res[0] == "Psalm 34:18;1 Peter 2:24 (ESV)"
