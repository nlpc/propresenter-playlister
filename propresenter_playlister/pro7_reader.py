from argparse import ArgumentParser, Namespace
from pathlib import Path
from typing import Sequence

from propresenter_playlister import __version__
from propresenter_playlister.protobuf.presentation_pb2 import (  # type: ignore
    Presentation,
)

# from propresenter_playlister.protobuf.playlist_pb2 import Playlist
# from propresenter_playlister.protobuf.library_pb2 import Library
from propresenter_playlister.protobuf.propresenter_pb2 import (  # type: ignore
    PlaylistDocument,
)


def main(argv: Sequence[str] | None = None) -> int:
    args = parse_args(argv)
    pro = read_pro_file(args.pro_file)
    print(f"{pro=}")
    return 0


def parse_args(argv: Sequence[str] | None = None) -> Namespace:
    parser = ArgumentParser()
    parser.add_argument("-V", "--version", action="version", version=__version__)
    parser.add_argument("pro_file")
    return parser.parse_args(argv)


def read_pro_file(pro_file: str) -> Presentation:
    # presentation = Playlist()
    # presentation = Library()
    # presentation = Presentation()
    presentation = PlaylistDocument()
    presentation.ParseFromString(Path(pro_file).read_bytes())
    return presentation


if __name__ == "__main__":
    raise SystemExit(main())
