from datetime import date

from propresenter_playlister import cfg


def test_load_config():
    _cfg = cfg.load_config("service-1.yml", date.today())
    assert cfg.dict_key(_cfg.playlist[0]) == "Announcements - Welcome"


def test_list_to_dict():
    lst = [{"attrs": {"name": "foo"}}, {"attrs": {"name": "bar"}}]
    d = cfg.list_to_dict(lst, lambda e: e["attrs"]["name"])
    assert d == {"foo": {"attrs": {"name": "foo"}}, "bar": {"attrs": {"name": "bar"}}}
