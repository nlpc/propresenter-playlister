# propresenter-playlister

Utility to generate ProPresenter 6 slides for New Life Presbyterian Church
of Orange County's Sunday worship.

# Installation

```
pip install propresenter-playlister --extra-index-url https://__token__:<your_personal_token>@gitlab.com/api/v4/projects/32874778/packages/pypi/simple
```

# Prerequisites

You will need to set the following environment variables in order to
access the various integrations with Planning Center Online API, Crossway's
ESV Bible API, and Dropbox API:

| Variable | Description |
| ------------ | ------------- |
| DROPBOX_KEY | for song slides that have been synced to dropbox |
| DROPBOX_SECRET | used in conjunction with DROPBOX_KEY |
| DROPBOX_REFRESH_TOKEN | long-lived token to refresh oauth2 tokens |
| ESV_TOKEN | for Bible verse refs |
| GL_TOKEN | for CI-controlled repo updates like version bumping and stable branch merges |
| PCO_APP_ID | for Planning Center service order and notes |
| PCO_SECRET | used in conjunction with PCO_APP_ID |

(See [below](#see-also) for API documentation to these integrations)

# Usage

```
usage: pro6-playlister [-h] [-d DIR] [-D DATE]

optional arguments:
  -h, --help            show this help message and exit
  -d DIR, --dir DIR     the output folder for the generated .pro6plx file
                        (default `.`).
  -D DATE, --date DATE  plan date (default `2022-08-07`).
```

# See also

| API Resource | Documentation |
| ------------ | ------------- |
| Planning Center | https://developer.planning.center/docs/#/overview/authentication |
| ESV | https://api.esv.org/docs/passage-text/ |
| Dropbox | https://dropbox-sdk-python.readthedocs.io/en/latest/index.html |
| Dropbox (auth) | https://dropbox.tech/developers/migrating-app-permissions-and-access-tokens#implement-refresh-tokens |

# Notes

## Getting a Dropbox refresh token

1. [Create an API app](https://www.dropbox.com/developers/apps/create)
   - Scoped access; Full Dropbox access type
   - [x] account_info.read
   - [x] files.metadata.read
   - [x] files.content.read
   - [x] sharing.read
2. [Get offline authorization code](https://www.dropbox.com/developers/documentation/http/documentation#oauth2-token:~:text=Auth%20URL%20for%20code%20flow%20with%20offline%20token%20access%20type)
   - With <APP_KEY>, visit https://www.dropbox.com/oauth2/authorize?client_id=<APP_KEY>&token_access_type=offline&response_type=code
3. [Get refresh token]
   - ```
     curl https://api.dropbox.com/oauth2/token \
       -d code=<AUTHORIZATION_CODE> \
       -d grant_type=authorization_code \
       -d client_id=<APP_KEY> \
       -d client_secret=<APP_SECRET>
     ```

## .. on Google Protobuf

- https://github.com/greyshirtguy/ProPresenter7-Proto.git
   - Basis for propresenter_playlister/pb
