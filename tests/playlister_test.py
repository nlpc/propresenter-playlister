import re
import zipfile
from datetime import date

import lxml.etree
import pytest
import responses

from propresenter_playlister import cli as playlister

X_CUE = (
    r'<RVDocumentCue UUID="[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}" '
    r'displayName="[^"]*" actionType="0" enabled="false" timeStamp="0.000000" '
    r'delayTime="0.000000" '
    r'filePath="~/Documents/ProPresenter6/[^"]*.pro6" selectedArrangementID=""/>\s*'
)


@pytest.mark.usefixtures("fake_env", "api_responses", "fake_dropbox_responses")
@responses.activate
def test_create_playlist_from_pco_plan(tmpdir):
    p = playlister.create_playlist_for_plan(date(2022, 1, 23), "service-1.yml", tmpdir)
    with zipfile.ZipFile(p) as z:
        playlist, *slides = (z.read(f) for f in z.filelist)
    s = playlist.decode()
    assert re.search(
        r"RVPlaylistDocument.*RVPlaylistNode.*" + X_CUE, s, re.S
    ), f"got {s}"
    # assert re.search("Revelation 1:5-8", s), f"got {s}"
    # assert re.search(r"Revelation 1_5-8 \(ESV\).pro6", s), f"got {s}"
    assert re.search(r"Praise The Father Praise The Son.pro6", s), f"got {s}"
    assert re.search(r"Good And Gracious King.pro6", s), f"got {s}"
    assert re.search(r"HC Q74.pro6", s), f"got {s}"
    assert re.search(r"Luke 4_8 \(ESV\).pro6", s), f"got {s}"
    assert re.search(r"PC 20220123.pro6", s), f"got {s}"
    assert re.search(r"Ephesians 2_4-5 \(ESV\).pro6", s), f"got {s}"
    assert re.search(r"Your Will Be Done \(CityAlight\).pro6", s), f"got {s}"
    assert re.search(r"Revelation 19_1-10 \(ESV\).pro6", s), f"got {s}"
    assert re.search(r"Revelation 19_1-10 Notes.pro6", s), f"got {s}"
    assert re.search(r"Behold Him.pro6", s), f"got {s}"
    assert len(slides) == 14
    s = slides[8].decode()
    assert re.search(
        r"RVPresentationDocument.*RVTimeline.*timeCues.*mediaTracks.*groups.*RVSlideGrouping.*"
        r"slides.*RVDisplaySlide.*cues.*displayElements.*RVTextElement.*RVRect3D.*position.*"
        r"shadow.*stroke.*NSColor.*NSNumber.*NSString.*RTFData.*arrangements",
        s,
        re.S,
    ), f"got {s}"
    assert re.search(
        r'CCLISongTitle="Ephesians 2:4-5 \(ESV\)".*'
        r'RVBibleReference.*bookIndex="48".*bookName="Ephesians".*'
        r'chapterEnd="2".*chapterStart="2".*verseEnd="5".*verseStart="4".*'
        r'RVSlideGrouping.*name="Ephesians 2:4-5".*'
        r'RVDisplaySlide .*label="Ephesians 2:4 \(ESV\)".*'
        r'RVDisplaySlide .*label="Ephesians 2:5 \(ESV\)".*',
        s,
        re.S,
    ), f"got {s}"


@pytest.mark.usefixtures("fake_env", "api_responses", "fake_dropbox_responses")
@pytest.mark.parametrize(
    "d",
    (
        pytest.param(date(2022, 5, 22), id="fail safe null PC"),
        pytest.param(date(2022, 6, 5), id="Lord's Supper"),
    ),
)
@responses.activate
def test_create_playlist_from_pco_plan_edge_cases(d, tmpdir):
    p = playlister.create_playlist_for_plan(d, "service-1.yml", tmpdir)
    assert p.endswith(f"{d.strftime('%F')}.pro6plx")


@pytest.mark.usefixtures("fake_env", "api_responses", "fake_dropbox_responses")
@responses.activate
def test_create_playlist_cof_not_qa(tmpdir):
    d = date(2022, 6, 5)
    p = playlister.create_playlist_for_plan(d, "service-1.yml", tmpdir)
    with zipfile.ZipFile(p) as z:
        slides = (f for f in z.filelist[1:] if f.filename.endswith(".pro6"))
        titles = [lxml.etree.fromstring(z.read(s)).get("CCLISongTitle") for s in slides]
    assert "The Apostle's Creed" in titles
