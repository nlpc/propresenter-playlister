import base64
import io
import json
import os
import urllib.request
from collections import namedtuple
from pathlib import Path
from unittest import mock
from urllib.parse import unquote

import httplib2
import pytest
import responses
from responses import matchers

from testing.mock_http import urlopen_side_effect

DBX_URLS = namedtuple("DbxUrls", ["auth", "download", "shared", "s"])(
    auth="https://api.dropboxapi.com/oauth2/token",
    download="https://content.dropboxapi.com/2/files/download",
    shared="https://content.dropboxapi.com/2/sharing/get_shared_link_file",
    s="https://www.dropbox.com/s",
)
DBX_DEFAULT_PATH = "/NLPC ProPresenter 2018/Sync 2018 - 1st Service"
DBX_DOC_PATH = f"{DBX_DEFAULT_PATH}/__Documents/Default"
DBX_MEDIA_PATH = f"{DBX_DEFAULT_PATH}/New Media/God's Invitation to Rest"
DBX_MP4_PATH = f"{DBX_MEDIA_PATH}/ProPresenter files_2025 God's Invitation to Rest"
SNAPSHOTS = "testing/snapshots"


@pytest.fixture
def fake_dropbox_responses():
    responses.post(DBX_URLS.auth, json={"access_token": "fake_token", "expires_in": 60})
    _register_private(f"{DBX_MP4_PATH}/1 call to worship.mp4")
    _register_private(
        "/NLPC Media Assets/Social Media Graphics/2025/2025 Sermon Series"
        "/God's Invitation to Rest/ProPresenter Blank God's Invitation to Rest.png"
    )
    _register_song("dummy")
    _register_song("Praise The Father Praise The Son")
    _register_song("Good And Gracious King")
    _register_song("Your Will Be Done (CityAlight)")
    _register_song("Behold Him")
    _register_song("The Apostle's Creed", "")
    _register_song("Response to the Word", "")
    _register_shared("kvxh5tguc7ug74g/or%20%281200%20%C3%97%20630%20px%29.png")
    return DBX_URLS


def _register_private(url_path):
    dbx_arg = {"path": url_path}
    body = Path(f"{SNAPSHOTS}/dropbox/logo.png").read_bytes()
    _register_download(DBX_URLS.download, dbx_arg, body)


def _register_shared(url_path):
    dbx_arg = {"url": f"{DBX_URLS.s}/{url_path}?dl=0"}
    body = Path(f"{SNAPSHOTS}/dropbox/logo.png").read_bytes()
    _register_download(DBX_URLS.shared, dbx_arg, body)


def _register_song(song, title=None):
    title = song if title is None else title
    x_slide = Path(f"{SNAPSHOTS}/TestSong1.pro6").read_bytes()
    x_slide = x_slide.replace(b"Test Song", title.encode())
    x_dbx_arg = {"path": f"{DBX_DOC_PATH}/{song}.pro6"}
    _register_download(DBX_URLS.download, x_dbx_arg, x_slide)


def _register_download(url, dbx_arg, body):
    api_res = {
        ".tag": "file",
        "client_modified": "2022-07-22T06:26:38Z",
        "id": "id:a4ayc_80_OEAAAAAAAAAXw",
        "name": unquote(list(dbx_arg.values())[-1].split("/")[-1]),
        "rev": "a1c10ce0dd78",
        "server_modified": "2022-07-22T06:26:38Z",
        "size": 7212,
        "link_permissions": {
            "allow_comments": True,
            "allow_download": True,
            "can_allow_download": False,
            "can_disallow_download": False,
            "can_remove_expiry": False,
            "can_revoke": False,
            "can_set_expiry": False,
            "team_restricts_comments": False,
            "visibility_policies": [
                {
                    "policy": {".tag": "public"},
                    "resolved_policy": {".tag": "public"},
                    "allowed": False,
                    "disallowed_reason": {".tag": "permission_denied"},
                }
            ],
        },
        **dbx_arg,
    }
    hdrs = {"dropbox-api-result": json.dumps(api_res)}
    dbx_arg = {"Dropbox-API-Arg": json.dumps(dbx_arg)}
    responses.post(url, body, headers=hdrs, match=[matchers.header_matcher(dbx_arg)])


@pytest.fixture
def fake_gcloud_responses():
    gcloud_auth_url = "https://oauth2.googleapis.com/token"
    responses.post(
        gcloud_auth_url,
        json={
            "access_token": "fake_access_token",
            "expires_in": 3599,
            "scope": "https://www.googleapis.com/auth/spreadsheets.readonly",
            "token_type": "Bearer",
        },
    )


@pytest.fixture
def mock_httplib2_request():
    with mock.patch.object(httplib2.Http, "request") as m:
        content = Path(f"{SNAPSHOTS}/gcloud/sheet_content.json").read_bytes()
        response_json = Path(f"{SNAPSHOTS}/gcloud/sheet_response.json").read_text()
        response = json.loads(response_json)
        m.return_value = (httplib2.Response(eval(response)), content)
        yield m


@pytest.fixture
def fake_env():
    env = {
        "DROPBOX_KEY": "fake_dropbox_key",
        "DROPBOX_REFRESH_TOKEN": "fake_dropbox_refresh_token",
        "DROPBOX_SECRET": "fake_dropbox_secret",
        "ESV_TOKEN": "fake_esv_token",
        "GCLOUD_REFRESH_TOKEN": base64.b64encode(
            json.dumps(
                {
                    "token": "fake.jwt",
                    "refresh_token": "fake_refresh_token",
                    "token_uri": "https://oauth2.googleapis.com/token",
                    "client_id": "test_client_id.apps.googleusercontent.com",
                    "client_secret": "fake_client_secret",
                    "scopes": ["https://www.googleapis.com/auth/spreadsheets.readonly"],
                    "expiry": "2023-03-20T07:59:32.411117Z",
                }
            ).encode()
        ).decode(),
        "PCO_APP_ID": "fake_pco_app_id",
        "PCO_SECRET": "fake_pco_secret",
    }
    with mock.patch.dict(os.environ, env) as m:
        yield m


@pytest.fixture
def mock_urlopen():
    with mock.patch.object(urllib.request, "urlopen") as m:
        yield m


@pytest.fixture
def api_responses(mock_urlopen):
    pco_api = "https://api.planningcenteronline.com/services/v2"
    esv = (
        "https://api.esv.org/v3/passage/text/"
        "?include-footnotes=false&include-headings=false&include-short-copyright=false"
        "&include-selahs=false&indent-poetry=false&"
    )
    routes = {}
    for p in Path(f"{SNAPSHOTS}/pco").rglob("*.json"):
        q = "/".join((p.parent / p.stem).parts[3:])
        routes[f"{pco_api}/{q}"] = io.BytesIO(p.read_bytes())
    for p in Path(f"{SNAPSHOTS}/esv").rglob("*.json"):
        routes[f"{esv}q={p.stem}"] = io.BytesIO(p.read_bytes())
    mock_urlopen.side_effect = urlopen_side_effect(routes)
    return routes
