import json
import os
import re
import urllib.request
from itertools import islice, tee
from typing import Any, Dict, List, Tuple

ESV_URL = (
    "https://api.esv.org/v3/passage/text/"
    "?include-footnotes=false&include-headings=false&include-short-copyright=false"
    "&include-selahs=false&indent-poetry=false&"
)


ScriptureCache = Dict[str, List[Tuple[str, str]]]
_scripture_cache: ScriptureCache = {}


def _cache_passage(verse: str) -> ScriptureCache:
    global _scripture_cache
    if verse not in _scripture_cache:
        all_verses = []
        for passage in q(verse)["passages"]:
            ref, _, verses = (s.strip() for s in passage.partition("\n") if s)
            book_chapter = ref.split(":")[0]
            nums, txts = tee(re.split(r"\[(\d+)\]\s", verses)[1:])
            current_book, chapter = book_chapter.rsplit(" ", 1)
            current_chapter = int(chapter)  # Convert chapter to an integer
            for num, txt in zip(islice(nums, 0, None, 2), islice(txts, 1, None, 2)):
                num = int(num)  # Convert the verse number to an integer
                if num == 1:  # Check if verse is the start of a new chapter
                    current_chapter += 1  # Increment the chapter
                a_ref = f"{current_book} {current_chapter}:{num}"
                _scripture_cache[a_ref] = [(txt.strip(), a_ref)]
                all_verses += _scripture_cache[a_ref]
        if len(all_verses) > 1:
            _scripture_cache[re.sub(r"–", "-", verse)] = all_verses

    return _scripture_cache


def get_passage(verse: str) -> List[Tuple[str, str]]:
    return _cache_passage(verse)[verse]


def q(verse: str) -> Dict[str, Any]:
    apikey = os.environ["ESV_TOKEN"]
    hdr = {"Authorization": f"Token {apikey}"}
    url = f"{ESV_URL}q={verse.replace(' ', '+')}"
    req = urllib.request.Request(url, headers=hdr)
    resp = urllib.request.urlopen(req)  # nosec
    return json.load(resp)  # nosec
