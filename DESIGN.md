# Playlister

The purpose of this app is mainly to produce a ProPresenter slideshow for a Sunday morning
worship. The slideshow will follow a well-defined liturgy as prepared on Planning Center
Online.

Normally, preparing the slideshow is a somewhat complicated manual process of
- copy/pasting slides from previous Sundays
- creating and arranging song slides
- creating slides for recited prayers
- creating slides for prepared responses
- creating slides for verses
- creating slides for quotes
- creating slides for prepared images
the content of which comes from Planning Center Online, Dropbox, and Google Drive.
This can be a very error prone process especially as it requires an operational understanding
of the ProPresenter software as well as access to the various information sources described.

Playlister strives to eliminate this burden by automatically generating the slides in the
expected format. This will facilitate onboarding of volunteers and allow for a more
accommodating weekly rotation schedule.

# Input sources

## Planning Center Online (PCO)

The main input will come from Planning Center Online. The staff will populate each week's
liturgy here. This includes:
- Bible verses for the Call to Worship
- Songs of Praise including their titles and arrangements
- Apostles' Creed or catechism questions and answers
- Bible verses as a Reading of the Law
- Public confession prayers to be recited corporately
- Bible verses as a Declaration of Pardon
- Song of Joy including title and arrangement
- Bible verses for the Preaching and Teaching of the Word
- References verses and quotes related to the sermon
- Song of Thanksgiving including title and arrangement
- Song of Doxology
- Announcements including images slides

## Dropbox

Much of the content will be provided by our Dropbox account. The staff will populate
- background image for the slides
- transition videos for the liturgy elements of the service order
- a sermon series image
- images meant for Announcements
- songs that have been pre-arranged

## ESV

PCO entries for Bible verse references will be resolved via the ESV organization's API
(https://api.esv.org/docs/passage-text).

## Google Cloud

Links to new images meant for post-worship Announcements will be provided by staff in our
Google Sheets page. These links are usually directed to our Dropbox account.

## Configuration YAML

See service-1.yml as an example. This file provides meta data for Playlister to define
paths relevant to the locations of the Dropbox artifacts, the Google Sheets url, and any
hints needed to parse PCO entries

Note: this configuration was originally designed to dictate the playlist and the various
input sources above would only supplement the configuration to produce the final playlist.
However, this would result in a rigid playlist that would need to be updated if ever the
playlist needed to be customized for occasions. So instead, the playlist will be dictated
by the PCO plan and the configuration will supplement the PCO plan.

## Local Files

Files that have already been downloaded will be used over online input sources.
