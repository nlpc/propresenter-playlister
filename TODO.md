| [ ] | as of    | done      | description |
| --- | -------- | --------  | ----------- |
| [ ] | 20240316 |           | Generate Pro7 playlists |
| [ ] | 20240316 |           | Countdown slide |
| [ ] | 20240316 |           | Infer pb values from sample slides |
| [ ] | 20240316 |           | Background graphic from PCO |
| [ ] | 20240316 |           | Service order slides from background image |
| [ ] | 20240316 |           | Infer download paths for Announcements |
| [ ] | 20240316 |           | Detect duplicate Doxology |
| [ ] | 20240316 |           | config as gl var |
| [ ] | 20240316 |           | migrate to gh |
| [ ] | 20240316 |           | async downloads |
