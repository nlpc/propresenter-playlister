# https://github.com/asottile/all-repos/blob/master/testing/mock_http.py
def urlopen_side_effect(url_mapping):
    def urlopen(request, **kwargs):
        url = request if isinstance(request, str) else request.get_full_url()
        return url_mapping[url]

    return urlopen
