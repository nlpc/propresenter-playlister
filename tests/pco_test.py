import urllib.request
from datetime import date
from unittest import mock

import pytest
import responses

from propresenter_playlister import cfg, pco

# @pytest.mark.usefixtures("fake_env", "api_responses")
# @pytest.mark.parametrize(
#     "_date, plan_id", [(date(2022, 1, 21), "55030718"), (date(2022, 1, 28), "55030719")]
# )
# def test_sunday_plan_needs_upcoming_sunday_plan_id_for_service_id(_date, plan_id):
#     with mock.patch("propresenter_playlister.pco.date") as m:
#         m.today.return_value = _date
#         items, notes = pco.get_sunday_plan()
#     assert len(items) > 20
#     assert len(notes) == 1


@pytest.mark.usefixtures("fake_env", "api_responses", "fake_dropbox_responses")
@responses.activate
def test_pco_get_plan_items_in_order():
    d = date(2022, 1, 23)
    _cfg = cfg.cfg("service-1.yml", d)
    items, _ = pco.get_plan(_cfg)
    assert [list(dict(d).keys())[0] for d in items] == [
        "Announcements - Welcome",
        "Service Order - Call to Worship",
        "Call to Worship",
        "Service Order - Song of Praise",
        "Praise The Father Praise The Son",
        "Good And Gracious King",
        "Service Order - Confession of Faith",
        "Confession of Faith",
        "Service Order - Reading of the Law",
        "Reading of the Law",
        "Service Order - Public Confession",
        "Public Confession",
        "Service Order - Declaration of Pardon",
        "Declaration of Pardon",
        "Service Order - Song of Joy",
        "Your Will Be Done (CityAlight)",
        "Service Order - Means of Grace",
        "Sermon Series",
        "Reading of the Word",
        "Response to the Word",
        "Preaching of the Word",
        "Service Order - Sacraments",
        "Service Order - Song of Thanksgiving",
        "Behold Him",
        "Service Order - Doxology",
        "Doxology",
        "Service Order - Benediction",
        "Announcements - Visitors",
        "Announcements - gsheets",
        "Announcements - Exit",
    ]


@pytest.mark.usefixtures("fake_env", "mock_urlopen")
def test_pco_api_authenticates_from_environment():
    pco.setup_auth()
    hdlrs = urllib.request._opener.handlers  # type: ignore
    AuthClazz = urllib.request.HTTPBasicAuthHandler
    dflt_auth_hdlr = [h for h in hdlrs if isinstance(h, AuthClazz)][0]
    creds = next(iter(dflt_auth_hdlr.passwd.passwd[None].values()))  # type: ignore
    assert creds == ("fake_pco_app_id", "fake_pco_secret")


@pytest.mark.parametrize(
    "d, sunday",
    [
        (date(2022, 1, 8), date(2022, 1, 9)),
        (date(2022, 1, 9), date(2022, 1, 9)),
        (date(2022, 1, 10), date(2022, 1, 16)),
    ],
)
@mock.patch("propresenter_playlister.pco.date")
def test_upcoming_sunday(mock_date, d, sunday):
    mock_date.today.return_value = d
    mock_date.side_effect = date
    assert pco.upcoming_sunday() == sunday
