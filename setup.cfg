[metadata]
name = propresenter_playlister
version = attr: propresenter_playlister.__version__
description = NLPC ProPresenter playlist generator
long_description = file: README.md
long_description_content_type = text/markdown
url = https://github.com/briandcho/propresenter_playlister
author = Brian Cho
author_email = briandcho@yahoo.com
license = MIT
license_files = LICENSE
classifiers =
    License :: OSI Approved :: MIT License
    Programming Language :: Python :: 3
    Programming Language :: Python :: 3 :: Only

[options]
packages = find:
install_requires =
    dropbox
    google-api-core
    google-api-python-client
    google-auth
    google-auth-httplib2
    google-auth-oauthlib
    googleapis-common-protos
    jinja2
    lxml<5.1.0
    ruamel.yaml
    setuptools
python_requires = >=3.9

[options.packages.find]
exclude =
    tests*
    testing*

[options.entry_points]
console_scripts =
    pppl=propresenter_playlister.cli:main

[coverage:run]
plugins = covdefaults
omit =
    propresenter_playlister/protobuf/*

[coverage:report]
fail_under = 99

[flake8]
description =
    Ignore the following:
    - E203 whitespace before ":"
    - W503 line break before binary operator
exclude = .tox,venv,propresenter_playlister/protobuf/*
ignore = E203,W503
max-line-length = 100

[mypy]
check_untyped_defs = true
disallow_any_generics = true
disallow_incomplete_defs = true
disallow_untyped_defs = true
no_implicit_optional = true
allow_redefinition = true

[mypy-testing.*]
disallow_untyped_defs = false

[mypy-tests.*]
check_untyped_defs = false
disallow_untyped_defs = false
disable_error_code = call-arg

[semantic_release]
version_variable = propresenter_playlister/__init__.py:__version__
commit_subject = bump: {version}
hvcs = gitlab
branch = main

[tox:tox]
envlist = py,pre-commit
toxworkdir = {env:TOXWORKDIR:.tox}

[testenv]
deps =
    covdefaults
    coverage
    pytest
    responses
commands =
    coverage erase
    coverage run -m pytest --junitxml=junit.xml -vv {posargs:}
    coverage report -im
    coverage xml -i
    coverage html -i
passenv = DROPBOX_REFRESH_TOKEN,DROPBOX_KEY,DROPBOX_SECRET,GCLOUD_SECRET,GCLOUD_REFRESH_TOKEN

[testenv:pre-commit]
deps = pre-commit
skip_install = true
commands = pre-commit run --all-files --show-diff-on-failure
passenv = PRE_COMMIT_HOME

[testenv:release]
deps =
    python-semantic-release==7.34.6
    setuptools
passenv =
    GIT_COMMITTER_NAME
    GIT_COMMITTER_EMAIL
    GL_TOKEN
    REPOSITORY_USERNAME
    REPOSITORY_PASSWORD
    REPOSITORY_URL
commands = semantic-release {posargs:publish}
