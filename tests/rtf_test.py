import re

from propresenter_playlister import cfg, rtf


def test_rtf_doc_has_pro6_header():
    cfg.cfg("service-1.yml")
    doc = rtf.create_doc("")
    assert re.match(r"{\\rtf.*Lato.*}", doc, re.S), doc


def test_rtf_bold_bullet():
    bullet = rtf.bold_bullet("foo")
    assert bullet == r"\f0\b\fs116 \cf2 foo"


def test_rtf_regular_bullet():
    bullet = rtf.regular_bullet("foo")
    assert bullet == "\\f1\\b0 \\\n\\\nfoo"


def test_rtf_bullet():
    bullet = rtf.bullet("foo")
    assert bullet == "\\\nfoo"


def test_rtf_regular_text():
    txt = rtf.regular_text("foo'")
    assert txt == r"\f1\fs112 \cf2 foo\'27"


def test_superscript():
    txt = rtf.superscript("42")
    assert txt == r"\f1\fs56 \cf2 \super2 42"


def test_unscript():
    txt = rtf.unscript("some text")
    assert txt == r"\fs112 \nosupersub some text"
