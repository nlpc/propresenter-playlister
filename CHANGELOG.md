# Changelog

<!--next-version-placeholder-->

## v0.4.25 (2025-01-26)

### Fix

* Expect 'ref txt' to be 'txt - ref' ([`7193f59`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/7193f5926bc1e589dc7f0f23e54244fd1429385d))
* No longer support 'ref, txt' ([`9f7f3f7`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/9f7f3f7984b1ac39a7981faf1fe2ae55c60cf805))

## v0.4.24 (2025-01-19)

### Fix

* Update theme - God's Invitation to Rest ([`56e968d`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/56e968da4bcdb2aa24d0ab2f6b24c72135659d2a))

## v0.4.23 (2024-12-29)

### Fix

* **#6:** Let ESV API handle validation ([`f810061`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/f810061b6bf13a53ce3fb4a38073c8254c0b117e))

## v0.4.22 (2024-12-08)

### Fix

* Sermon series - advent ([`822a89a`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/822a89a1b8bfe0ab169d491de5ccef91fd00d7a3))

## v0.4.21 (2024-09-08)

### Fix

* Update theme - God Is ([`ba88f6d`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/ba88f6d9598702541e0098eddc7dd34eefd63beb))

## v0.4.20 (2024-07-07)

### Fix

* Handle quoted titles ([`23a9d3b`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/23a9d3bfd88f2c531300cc98d53d7508a11ea472))
* Update theme - Psalms for the Summer 2024 ([`41735d2`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/41735d2712a3de324c9ced94f707f884e617c793))

### Documentation

* Local cache approach ([`485faf8`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/485faf8d548602165aa6404faf8443591eacdbc8))
* TODO DESIGN ([`e474d8c`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/e474d8cbd1c004fb3d930a5283bc07340b928a11))
* **pro7:** Notes on protobuf ([`13c77f3`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/13c77f3bd0e5b748c4f07e30a1ed8da9984c88f7))

## v0.4.19 (2024-05-05)

### Fix

* Revert to Grace Made Visible ([`9243ce6`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/9243ce6945f5d8a3239d250f8cb6da2b293a628f))

## v0.4.18 (2024-04-07)

### Fix

* Sermon series file name ([`e39205f`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/e39205f82eff43127c85bf2137620934f52a77ce))

## v0.4.17 (2024-04-07)

### Fix

* Sermon series duplicate ([`8f92afa`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/8f92afa8fbc9da681ba8281a65ab668301c98b6f))

## v0.4.16 (2024-04-07)

### Fix

* Update sermon graphic: misions month ([`d605ab2`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/d605ab2a07002de6904a670723e3efd96ea198e1))
* Update year path ([`a19b8d9`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/a19b8d9f387b82a3845fa30576488fd0ed1bc19e))

## v0.4.15 (2023-12-31)

### Fix

* Update theme - Grace Made Visible ([`04f403b`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/04f403b050051955a2ee4ddaff409a3aa3bee904))

## v0.4.14 (2023-12-24)

### Fix

* Update theme - Christmas 2023 ([`db2f797`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/db2f7974586d2d87800143223b425c8561f4e7a0))

## v0.4.13 (2023-12-03)

### Fix

* Update theme - Liturgy for Life ([`2b07b42`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/2b07b42cc6ff72fe38d54635fb99848b304c3bc6))

## v0.4.12 (2023-11-05)

### Fix

* Update theme - Elders and Deacons ([`56ff949`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/56ff949fd68d7b2593fc7117efeca1919684d451))

## v0.4.11 (2023-10-04)

### Fix

* **release:** Setuptools dep ([`81c234e`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/81c234e6ae16bace390c150c310225673de0d875))

## v0.4.10 (2023-10-04)

### Fix

* Newline typo ([`a3b1cef`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/a3b1cefe0c0be2432fa39cd8b68f0761cc52ee0f))
* Update theme: To Belong ([`d2ff877`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/d2ff877aea43cd9a468e14177dac7963d8ab991d))

## v0.4.9 (2023-09-09)

### Fix

* Update theme - Core Values ([`dc89684`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/dc89684f227079cb9c2269da5fb39f76a104b89d))

## v0.4.8 (2023-08-31)

### Fix

* Update theme - Vision ([`3b0211c`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/3b0211c0e9ec035164d6c210351f8adb93dc88f1))

## v0.4.7 (2023-08-23)

### Fix

* Song groupings ([`99eae8c`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/99eae8c626a5a9281351b74c34755608f6b03d1d))
* Announcements - Visitors ([`5b98164`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/5b9816431a0f7185c8f6bc1d8e7e0f30f25cc3a2))

## v0.4.6 (2023-08-12)

### Fix

* Non-contiguous verses ([`437879f`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/437879f869be993e9f536a769db066c8e9d7caaf))

## v0.4.5 (2023-08-12)

### Fix

* Whole chapters preserve verse numbers ([`a347422`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/a3474225e770c726cb469d63f749240ef5b6004f))

## v0.4.4 (2023-08-11)

### Fix

* Set background image ([`92c3a27`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/92c3a2776cd1ad9b2c69031d8d81ea52b8b62b67))
* Download_only ([`4b9f4b9`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/4b9f4b9dccae49f31f85925485c9ae907d4f8d1f))

## v0.4.3 (2023-08-10)

### Fix

* Parse new songs ([`046954e`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/046954e37fe4f9ba77dbe6e2e9812ad7baef9ec3))

## v0.4.2 (2023-07-30)

### Fix

* Service order labels ([`9c97f5a`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/9c97f5a8cd95cc8227ba06a84bfe44c905de9dd4))

## v0.4.1 (2023-07-30)

### Fix

* Normalize dropbox ([`d53a084`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/d53a084d3e7225c596240c35360025a3d2c8ef08))

## v0.4.0 (2023-07-09)

### Feature

* Dropbox media path ([`7707b87`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/7707b87690e679aa583008eba464935c4c201d27))

### Fix

* Granular service orders ([`b212f67`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/b212f67820b1ff868edf8271a1e3261e193ec730))

## v0.3.4 (2023-06-11)

### Fix

* Add song arrangement ([`4572444`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/457244428fee57365811acfbb6b638a6d28787d3))

## v0.3.3 (2023-06-09)

### Fix

* Honor verse newlines ([`37ddb70`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/37ddb70d24ed46f31f909a8af7c6f478ed592086))
* Add announcement png ([`061dc17`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/061dc17c721d80a3968296bd56ecee79687934fc))

## v0.3.2 (2023-05-21)
### Fix
* Account for whole Bible chapter ([`e7ba2b0`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/e7ba2b00657bdc87cf79e10d09419c1a58b06419))

### Documentation
* Notes on getting Dropbox refresh token ([`ff4f42a`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/ff4f42a5795ed0385c1dbd50f7388d95f9916060))

## v0.3.1 (2023-04-02)
### Fix
* Utf-8 hyphen regression ([`a9b9b6f`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/a9b9b6f213d766be16267b68c703d03cc7a55e97))

## v0.3.0 (2023-03-21)
### Feature
* Configurable parsers ([`d3ed16f`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/d3ed16fb58317d1c285e2c90c0d01512f076e8ba))
* Add -c ([`65cff52`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/65cff5206125dfeb5b7f035f0911c4676b634fab))

### Fix
* Account for verse across chapter range ([`99e9b23`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/99e9b237268762c20a72414c5b167b58d96da850))
* Rm single quote from pro6 filename; set empty title ([`1e98603`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/1e98603e715e73182c2df4841321d5ffa36d9095))
* Non-qa confession ([`3b0d876`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/3b0d876332cce55555391242d462ac424a2b6d9c))
* Default parse_ prefix ([`cf7473f`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/cf7473fd12e57f03bafb994baeb9c8c9b40873d3))
* Default parser namespace ([`52749f2`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/52749f2b8a0978b6d0d938f1419c585acde7491a))
* Configurable dropbox path ([`77dcdfc`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/77dcdfcfabc7592be9901cbf7e0d8427afd5e1c0))
* Retrieve pco 2nd svc ([`ed1e147`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/ed1e1474e9555ca4eab3398e1d71c9c953159e23))

## v0.2.3 (2022-11-04)
### Fix
* Account for occasional sacraments ([`c837ccd`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/c837ccd5947bd455954d7ee74680fab2b2583766))

### Documentation
* Replace pyenv description with tox usage ([`f24034d`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/f24034d888deb591f1c11ee376a40223446f26e0))

## v0.2.2 (2022-10-22)
### Fix
* Replace long hyphen with ascii hyphen ([`6db2d7c`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/6db2d7c7922f4dd3e72d75721b5090e821319bfc))

## v0.2.1 (2022-09-24)
### Fix
* Parse Psalm 118:24,28-29 ([`6d205e7`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/6d205e7c6f447a6c177dc2246827e5d9e8a49e0a))

### Documentation
* Contributing ([`6680736`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/6680736b3d5d0dff1c0bb07634b0b9e4e18436f4))

## v0.2.0 (2022-08-09)
### Feature
* Song slides from dropbox ([`e8cbdb1`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/e8cbdb1733dce5d082afd9beadb9ab8fe06173af))

### Fix
* Handle em and en dash rtf char ([`71a3445`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/71a344535bc454c7a26fc05b3cdd38f7ee753568))
* Split verses ([`209190d`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/209190dee98c00c230c4fcf60cb3d39beee8b870))
* ENABLE_WHITE_ON_BLACK applies to songs ([`1ae9046`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/1ae9046e640f4c07e5f760d7c595ee6bee9b1f14))
* Book/chapter/no verse ([`3765a87`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/3765a87ae75c0948e4911321ad8f17a7e9ca174f))

## v0.1.0 (2022-07-09)
### Feature
* White on black; complete notes parsing ([`639a01d`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/639a01dd37776c4963b8299da6c722d5ff3309a5))
* **wip:** Generate passage slides ([`95add35`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/95add3527b6386cdbefa2a119b8e6e5fee42beb3))

### Fix
* Allow space after 'Q' in HC ([`6310eb3`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/6310eb36e6500f3cca1e4ecf8ad209085fb3d441))
* Sub verses ([`75c0807`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/75c080776ac302b7040f3c9aa1a9aff77b9f015e))
* Left apostrophe; suppress esv annotations ([`33e5c85`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/33e5c856c1eaa0d25dd016a2170592ef088c2b03))
* Lord's supper and missing plan item ([`f840af3`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/f840af3d4a666eff7d83498ebacd0c63f498fc86))
* Fail safe when parsing plan items ([`599f406`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/599f406b21227f111c47b1baee8f640275cbe89f))
* Fail safe if no description for parseable item ([`5eae2fc`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/5eae2fc4959eb547886d3c6d2ef57d7ae2295ed3))
* Verse number ([`9709cae`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/9709cae203c3f500b6a6c71fde0152daaf45468e))
* Handle empty sermon description ([`26978b3`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/26978b390ba333369c945e6e67515ca123458a58))
* Rm '- Revelation' suffix from Service Order ([`0c593a4`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/0c593a46adad79f60a2079a89abebddb569bb351))
* Leave xml quotes as is; do not truncate titles ([`0797898`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/0797898121198c410677ec941216fbff300a1138))

### Documentation
* API notes ([`79aa3c3`](https://gitlab.com/nlpc/propresenter-playlister/-/commit/79aa3c36a1869375d6ff6b754f8d6b9ff1ba7550))
