import argparse
import importlib
import logging
from datetime import date, datetime
from typing import Optional, Sequence

from . import pco
from .cfg import cfg, dict_item
from .fntools import first


def create_playlist_for_plan(d: date, config_path: str, outdir: str) -> str:
    _cfg = cfg(config_path, d)
    playlist, plan = pco.get_plan(_cfg)
    parsers = importlib.import_module(_cfg.parser_module or f"{__package__}.parsers")
    parsers.init(_cfg, outdir)
    for playlist_item in playlist:
        title, _parsers = dict_item(playlist_item)
        _parsers = [_parsers] if isinstance(_parsers, str) else _parsers
        _parsers = [getattr(parsers, f"parse_{p}") for p in _parsers]
        try:
            first(lambda: p(playlist_item, plan) for p in _parsers)
        except Exception as e:
            logging.warning(f"Error parsing {title}: {repr(e)}")
    return parsers.finalize(_cfg, outdir)


def main(argv: Optional[Sequence[str]] = None) -> int:
    default_date = pco.upcoming_sunday().strftime("%F")
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--config", default="playlist.yml", help="playlist template"
    )
    parser.add_argument(
        "-d",
        "--dir",
        default=".",
        help="the output folder for the generated .pro6plx file (default `%(default)s`).",
    )
    parser.add_argument(
        "-D", "--date", default=default_date, help="plan date (default `%(default)s`)."
    )
    args = parser.parse_args(argv)
    d = datetime.strptime(args.date, "%Y-%m-%d").date()
    print(f"Creating playlist for {args.date}")
    p = create_playlist_for_plan(d, args.config, args.dir)
    print(f"Created {p}")
    return 0


if __name__ == "__main__":  # pragma: no cover
    raise SystemExit(main())
